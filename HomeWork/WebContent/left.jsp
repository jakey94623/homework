<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
  <aside class="left-sidebar" data-sidebarbg="blue">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="Home.jsp" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">主頁</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="image.jsp" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span class="hide-menu">圖片上傳</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="excel.jsp" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">匯入檔案</span></a></li>
                       
                            </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
      
</body>
</html>