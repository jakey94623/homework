<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>檔案上傳</title>
    <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<jsp:include page="blink.jsp" flush="true" />
<body>  <div class="page-wrapper">
 <div class="page-breadcrumb">
				<div class="row">
					<div class="col-12 d-flex no-block align-items-center">

						<div class="ml-auto text-right">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="#">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Image
										Upload</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
     	 <h2 class="ui center aligned icon header">
        <i class="file excel outline icon olive"> </i> Please select Excel
    </h2>
<form action="/HomeWork/ExcelServlet" method="post"
          enctype="multipart/form-data">
          Excel  :<input type="file" name="ExcelInput" accept=".xls"/><br>
        <input type="submit" value="Upload" name="upload" />
    </form></div>>
 
</body>
</html>