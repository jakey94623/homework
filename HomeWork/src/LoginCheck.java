
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LoginCheck implements Filter {
	private FilterConfig filterConfig;
	public void init(FilterConfig fConfig) throws ServletException {
		this.filterConfig=fConfig;
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpSession session=((HttpServletRequest)request).getSession();
		response.setCharacterEncoding("gb2312");
		if(session.getAttribute("me")==null){
		PrintWriter out=response.getWriter();
		out.print("<script>alert('請登入!');location.href='../login1.htm'</script>");
		}
		else{
		// pass the request along the filter chain
		chain.doFilter(request, response);
		}
	}



	public void destroy() {

	}
}