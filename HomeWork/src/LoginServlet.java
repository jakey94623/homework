
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.db2.jcc.am.ResultSet;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String account = request.getParameter("account");
		String PS = request.getParameter("PS");
		System.out.println("輸入進來的" + account);
		System.out.println("輸入進來的" + PS);
		ArrayList<String> account1 = new ArrayList<String>();
		ArrayList<String> password = new ArrayList<String>();
		try {
			Class.forName("com.ibm.db2.jcc.DB2Driver");
			Connection con = DriverManager.getConnection("jdbc:db2://127.0.0.1:50000/HWDB", "db2admin", "1234");
			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM IMSOFT.\"USER\"";
			// String sql = "INSERT INTO IMSOFT.\"USER\" VALUES
			// ('JASON','JASON69','123456','JASON@gmail.com')"; 標準輸入
			// String sql = "INSERT INTO `USER`(`NAME`,`ACCOUNT`,`PASSWORD`,`EMAIL`) "+
			// "VALUES ( "+"\""+name+"\""+","+account+","+password2+","+email+")" ;
			ResultSet rs = (ResultSet) stmt.executeQuery(sql);
			while (rs.next()) {
				account1.add(rs.getString("ACCOUNT"));
				password.add(rs.getString("PASSWORD"));
			}
			System.out.println("name" + account1);
			System.out.println("password=" + password);
			int who = account1.indexOf(account);

			System.out.println(who);
			System.out.println(account.contains(account));
			System.out.println(password.contains(PS));
			if (who != -1) {
				if (account.contains(account) && password.contains(PS)) {
					if (account.equals(account1.get(who)) && PS.equals(password.get(who))) {
						response.sendRedirect("Home.jsp");
					} else {
						System.out.print("密碼比對錯誤");
						response.sendRedirect("error-403.html");
					}
				} else {
					System.out.print("帳號密碼錯誤");
					response.sendRedirect("error-405.html");
				}
			} else {
				System.out.print("找不到帳號");
				response.sendRedirect("error-404.html");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

}

//System.out.println(rs.getString("NAME") + "\t\t" + rs.getString("ACCOUNT") + "\t\t"
//		+ rs.getString("PASSWORD") + "\t\t" + rs.getString("EMAIL"));
